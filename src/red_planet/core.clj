(ns red-planet.core
  (:require
   [clojure.string :as string]
   [meander.epsilon :refer [match] :as m]))

(defn constant? [operation]
  (or (string? operation)
      (number? operation)))

(defn value? [operation]
  (when (symbol? operation)
    (string/starts-with? (name operation) "!")))

#_(value? (symbol "!v1"))
#_(value? (symbol "!v1+"))

(defn assignment? [operation]
  (and (value? operation)
       (string/ends-with? (name operation) "+")))

#_(assignment? (symbol "!v1+"))
#_(assignment? (symbol "!v1"))

(defn extract-cond [ops]
  (let [else-pos (.indexOf ops 'else>)
        [if [_ & else]] (split-at else-pos ops)]
    [if else]))

(defn lookup [env name]
  (let [return (get env name ::OOPS!)]
    (when (= ::OOPS! return)
      (throw (ex-info "variable doesn't exist in environment:" {:lookup name})))
    return))

(defn interpret [operation stack env]
  (match operation
    (m/pred constant? ?constant) [(conj stack ?constant) env]

    (m/pred assignment? (m/symbol ?assignment))
    [stack (assoc env
                  (symbol (subs ?assignment 0 (dec (count ?assignment)))) (peek stack))]

    (m/pred value? (m/symbol _ ?value :as ?variable))
    [(conj stack (lookup env ?variable)) env]

    (invoke> ?op (m/pred pos? ?arity))
    (let [[new-stack args] (split-at (- (count stack) ?arity) stack)
          op (resolve ?op)
          push (apply op args)]
      [(conj (vec new-stack) push) env])

    (if> & ?body) (let [[if-body else-body] (extract-cond ?body)
                        test (peek stack)
                        new-stack (pop stack)]
                    (->> (if test if-body else-body)
                         (reduce (fn [acc op]
                                   (apply interpret op acc))
                          [new-stack env])))
    <pop> [(pop stack) env]
    _ (throw (ex-info "invalid stack operation:" {:operation operation}))
    )
  )

#_(interpret '!a                 []  {'!a 1})
#_(interpret '!b                 []  {'!a 1})
#_(interpret '!a+                [2] {'!a 1})
#_(interpret 1                   [2] {'!a 1})
#_(interpret "test"              [2] {'!a 1})

#_(interpret '<pop>              [2] {'!a 1})
#_(interpret '<pop>              [2 4] {'!a 1})

#_(interpret (list 'invoke> * 2) [2 3] {})
#_(interpret (list 'invoke> = 2) [2 2] {})
#_(interpret (list 'invoke> = 2) [2 3] {})

#_(interpret (list 'if>
                     '!v1
                     '!v2
                     '(invoke> - 2)
                     'else>
                     "false!!"
                     '(invoke> println 1)
                     '<pop>
                     '!v1
                     '!v2
                     '(invoke> * 2)
                     ) [false] {'!v1 3 '!v2 8})

#_(interpret (list 'error> = 2) [2 3] {})

(defmacro defstackfn [name init-env & operations]
  `(defn ~name ~init-env
     (let [env# ~(->> init-env (map (juxt #(concat `(symbol ~(str %))) identity)) (into {}))]
       (->>
        '[~@operations]
        (reduce
         (fn [acc# op#]
           (apply interpret op# acc#))
         [[] env#])
        ffirst))))

(defstackfn f [!a !b !c]
  !a
  !b
  (invoke> + 2)
  !v1+
  !c
  !c
  <pop>
  2
  (invoke> * 2)
  !v2+
  (invoke> = 2)
  (if> ;; stack empty
      !v1
    !v2
    (invoke> - 2)
    else>
    "false!!"
    (invoke> println 1)
    <pop>
    !v1
    !v2
    (invoke> * 2)
    )
  )

#_(f 1 2 4)

;; prints "false!!" returns 24
